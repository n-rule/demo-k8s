data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "k3s-master" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t3.medium"
  iam_instance_profile   = aws_iam_instance_profile.ssm-profile.name
  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.public-sg.id]
  user_data              = data.cloudinit_config.cloudinit.rendered

  tags = {
    Name = "${var.project-name}-master"
    Terraform = "true"
    Environment = var.project-name
  }
}

resource "aws_instance" "k3s-worker" {
  count = var.workers-count
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t3.micro"
  iam_instance_profile   = aws_iam_instance_profile.ssm-profile.name
  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.public-sg.id]
  #user_data              = data.cloudinit_config.cloudinit.rendered

  tags = {
    Name = "${var.project-name}-worker-${count.index}"
    Terraform = "true"
    Environment = var.project-name
  }
}
