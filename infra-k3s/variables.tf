variable "project-name" {
  default     = "demo-k3s"
  description = "default prefix for all resources"
}

#variable "vpc_private_subnets" {
#  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
#  type    = list(string)
#}

variable "vpc_public_subnets" {
  default = ["10.0.101.0/24"]#, "10.0.102.0/24", "10.0.103.0/24"]
  type    = list(string)
}

variable "workers-count" {
  default = 0
  type    = number
}