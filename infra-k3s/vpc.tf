module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.project-name}-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["eu-central-1a"]#, "eu-west-1b", "eu-west-1c"]
  #private_subnets = var.vpc_private_subnets
  public_subnets  = var.vpc_public_subnets

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform = "true"
    Environment = "demo"
  }
}

